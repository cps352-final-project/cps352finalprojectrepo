# Team Project #

University of Dayton

Department of Computer Science

CPS 352, Fall 2020

Instructor: Dr. Phu Phung

# Team 12 members

1. Wesam Haddad, <haddadw1@udayton.edu>
2. Shara Shrestha, <shresthas6@udayton.edu>
![logo](https://trello-attachments.s3.amazonaws.com/5fd2ecce6dd43a1bac392807/500x500/46097e8bb186764c5f72756de8fe36fa/Blocs.png) 
# Part 1: Language Design

## Regular Grammar
Here we establish our syntax and semantics for our programming language, including\n
local binding, basic operations and comparisions, conditional statements, function declaration and call statements,\n
and rules for printing out messages.
~~~

<expr> 	::= <expr> <primitive> <expr>
	   	::= <number> | <identifier>
		::= <callChapter> | <newChapter>
		::= <cond> | <var> | <string>
		::= <write>

<callChapter> 	::= read chapter <expr> main characters are [<expr>]+(,)
<newChapter> 	::= new chapter casting [<expr> as <identifier>]+(,) [ <expr> ]
<cond>	   		::= if <expr> then <expr> else <expr>	   
<var> 			::= let <identifier> be <expr> in <expr>
<write> 		::= write [<string>+(+)] 
<number>		::= 0-9 | <number>.<number> | - <number> | + <number>
<identifier> 	::= [a-zA-Z]+[a-zA-Z0-9]+
<string>        ::= "\"" [a-zA-Z]+[a-zA-Z0-9]+ "\""     
<primitive> 	::= plus | minus | divided by | times | less than | greater than | equal to | not equal to
	   			::= less than or equal to | greater than or equal to | to the power of
~~~

## Semantical Meaning
- **\<expr\>** 	= Any type of expression or line of code that can be written together and combined with others to do many different things   
- **\<write\>**	= This is the print statement and would output any string literals concatenated together   
- **\<var\>** 	= This is to create a local binding, or variable and assign an expression to a variable name (identifier)   
- **\<cond\>** = This is the conditional statement where you can have any number of if/elseif/else statements    
- **\<newChapter\>** = This is how you create a new funciton and , the "casting" is where you create the function parameters and define what the function does in its body {}   
- **\<callChapter\>** = This is how you call a predefined function and the "main characters" are where you pass in your arguments and values into the function call   
- **\<number\>** = This is where you can have integers, floats, and postive and negetive numbers   
- **\<identifier\>** = This is how you can name different types of expressions that always need to start with a letter, and can be followed by numbers   
- **\<primitive\>** = This represents all the operands, and comparison operators, (+, -, /, *, >, <, <=, >=, ==, !=)   
- **\<string\>** 	= This allows us to create string literals that can be printed out


## Program Examples
Below are a few examples of how programs or books will be written in Blocs, our language.

~~~
let apple1 be 5,
        apple2 be 10 
        in
            if [apple1 equal to 3] 
                then 
                    write["Apple 1 is the apple for you"]
                else 
                    write["Apple 2 is the apple for you"]
~~~
~~~		
	[[[-3 plus +205] divided by 2.3] times 3.4]
~~~
~~~	
if [[9 minus 8] less than 5] 
		then
			write["Hi!"]
		else
			write["Bye!"]
~~~
~~~		
	read chapter 
		new chapter casting [length, width][
		[length times width]
		]
	main characters are [5, 3]			
~~~

# Part 2: Language Implementation and Evaluation

## Language Implementation
To implement our programming language, Blocs, we translated our regular grammar shown above using\n 
SLLGEN a parser generation tool. This allowed for us to check all of the tokens a user inputs and checks if it \n
fits our grammar rules.

~~~
(define the-lexical-spec
  '((whitespace (whitespace) skip)
    (comment (";" (arbno (not #\newline))) skip)
    (identifier
      (letter (arbno (or letter digit "_" "-" "?")))
      symbol)
    (number ((or "" "-" "+") (arbno digit) (or "." "") (arbno digit)) number)
    (string ("\"" letter (arbno (or letter digit "_" "-" "?" " " "!" "@" "#" "$" "%" "^" "&" "*" "-" "+" "<" ">")) "\"") string)
   )
)

(define the-grammar
  '((program (myexpression) a-program)
    (myexpression (number) lit-exp)
    (myexpression (identifier) id-exp)
    (myexpression ("\"" identifier "\"") string-exp)
    (myexpression (string) string-exp)
    (myexpression
      ("[" myexpression primitive myexpression "]")
      primapp-exp)
    (myexpression
      ("let" (separated-list identifier "be" myexpression ",") "in" myexpression)
      let-exp)
    (myexpression
     ("if" myexpression "then" myexpression "else" myexpression) if-exp)
    (myexpression
     ("new chapter" "casting" "[" (separated-list identifier ",") "]" "[" myexpression "]") function-def-exp)
    (myexpression
     ("read chapter" myexpression "main characters are" "[" (separated-list myexpression ",") "]") function-call-exp)
    (myexpression
     ("write" "[" (separated-list string "+") "]") print-exp)
    (primitive ("plus")     add-prim)
    (primitive ("minus")     subtract-prim)
    (primitive ("divided by") divide-prim)
    (primitive ("times")      mult-prim)
    (primitive ("less than")  lessThan-prim)
    (primitive ("greater than") greaterThan-prim)
    (primitive ("equal to")     equalTo-prim)
    (primitive ("not equal to") notEqualTo-prim)
    (primitive ("less than or equal to") lessOrEqual-prim)
    (primitive ("greater than or equal to") greaterOrEqual-prim)
    (primitive ("to the power of") power-prim)
   )
)
~~~

These different myexpression's corrolate to different grammar rules we had so that the parser, SLLGEN, can read them.


#### Key Steps   
- Changed keywords and expressions from original grammar to fit our new syntax:     
   • ~~def func~~  => *new chapter*     
   • ~~exec func~~ => *read chapter*     
- Added new operations to existing list:      
   • *divided by     
   • to the power of*   
- Added new grammar rules to allow for float and integer numbers
- Added new conditional primitives:      
   • *greater than or equal to      
   • less than*     
- Added new functionality to our programming language:    
   • *write* function: We can now display string literals concatinated together so people can return sentences           
- Added matching evaluation expressions in the interpreter       
   • print-exp => Using a helper function goes through the string literals in the list generated and prints them out
   
## Language Evaluation

### Program Examples
 
#### Conditional
 ![1](https://trello-attachments.s3.amazonaws.com/5fd2ecce6dd43a1bac392807/926x339/e791410a3b128bc24917acf0f31e92ff/1.PNG.png)
 
#### Operations
 ![2](https://trello-attachments.s3.amazonaws.com/5fd2ecce6dd43a1bac392807/929x206/03efbddd95a0da12bfd3154456be2799/2.PNG.png)
 
#### Conditional + Operations
 ![3](https://trello-attachments.s3.amazonaws.com/5fd2ecce6dd43a1bac392807/927x318/e82105f40596189b34219067c4f4aa04/3.PNG.png)
 
#### Functions Execution
 ![4](https://trello-attachments.s3.amazonaws.com/5fd2ecce6dd43a1bac392807/929x291/5778b97e835fb0f89b521eba691eb9ee/4.PNG.png)
 
### Program Error Handling

#### Binding Error
![5](https://trello-attachments.s3.amazonaws.com/5fd30bd6f31f6387c2450588/923x273/c83c3b2c365172e72ea803dba130c210/5.PNG.png)

#### Eval-Expression Error
![6](https://trello-attachments.s3.amazonaws.com/5fd30bd6f31f6387c2450588/932x250/637c8c0e5789d94fc00c2889d2fc3715/6.PNG.png)